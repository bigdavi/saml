<?php

function desencriptar($string)
{
	$key = '.Bigdavi$$$';
	$method = 'aes-256-cbc';
    $key = substr(hash('sha256', $key, true), 0, 32);
	$iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);
	
	$string=str_replace('@','+',$string);
	$string=str_replace('!!','/',$string);
	
	$decrypted = openssl_decrypt(base64_decode($string), $method, $key, OPENSSL_RAW_DATA, $iv);


return $decrypted;
}

function encriptar($string)
{
	$key = '.Bigdavi$$$';
	$method = 'aes-256-cbc';
    $key = substr(hash('sha256', $key, true), 0, 32);
	$iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);
	
		
	//$encrypted = str_replace('+','@',base64_encode(openssl_encrypt($string, $method, $key, OPENSSL_RAW_DATA, $iv)));
	
	$string=base64_encode(openssl_encrypt($string, $method, $key, OPENSSL_RAW_DATA, $iv));
	$string=str_replace('/','!!',$string);
	$string=str_replace('+','@',$string);
	$encrypted = $string;
	
return $encrypted;
}

function formatBytes($size, $precision = 2)
{
    $base = log($size, 1024);
    $suffixes = array('', 'KB', 'MB', 'GB', 'TB');   

    return round(pow(1024, $base - floor($base)), $precision) .' '. $suffixes[floor($base)];
}