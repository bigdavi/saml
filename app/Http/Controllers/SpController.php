<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use OneLogin\Saml2\Auth as Auth2;

class SpController extends Controller
{
    public function getSettings($entity)
    {
        // TODO: consultar credenciales dinamicas por empresa(entityId)
        $entityId = desencriptar($entity);
        $idpName = $entityId=='123'? 'sonda.json': null;

        $idpName = 'sonda.json';
        $idpContents = file_get_contents(base_path("/docs/idp/$idpName"));
        $idp = json_decode($idpContents, true);
        $settings = config('saml');
        $settings['idp'] = $idp;
        return $settings;
    }

    public function getAuth($entity)
    {
        return new \OneLogin\Saml2\Auth($this->getSettings($entity));
    }

    public function viewApp()
    {
        return view('sp.app');
    }

    public function viewHome()
    {
        if(!isset($_SESSION)){session_start();}
        $userData = $_SESSION['samlUserdata']?? null;
        if($userData){
            return view('sp.home');
        }else{
            return redirect('sp/app');
        }
    }

    public function viewMetadata()
    {
        try {
            $settings = config('saml');

            $auth = new \OneLogin\Saml2\Auth($settings);

            $settings = $auth->getSettings();

            $metadata = $settings->getSPMetadata();

            $errors = $settings->validateMetadata($metadata);
            if (empty($errors)) {
                header('Content-Type: text/xml');
                echo $metadata;
                exit;
            } else {
                throw new \OneLogin\Saml2\Error(
                    'Invalid SP metadata: '.implode(', ', $errors),
                    \OneLogin\Saml2\Error::METADATA_SP_INVALID
                );
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function login($entity)
    {
        session_start();

        $needsAuth = empty($_SESSION['samlUserdata']);

        if ($needsAuth) {

            $auth = $this->getAuth($entity);

            if (!empty($_REQUEST['SAMLResponse']) && !empty($_REQUEST['RelayState'])) {
                $auth->processResponse(null);
                $errors = $auth->getErrors();
                if (empty($errors)) {
                    // user has authenticated successfully
                    $needsAuth = false;
                    $_SESSION['samlUserdata'] = $auth->getAttributes();
                }
            }

            if ($needsAuth) {
                // $auth->login();
                // $auth->login($returnTo, $parameters, $forceAuthn, $isPassive, $stay, $setNameIdPolicy, $nameIdValueReq)
                $ssoBuiltUrl = $auth->login(null, [], false, false, true);
                $_SESSION['AuthNRequestID'] = $auth->getLastRequestID();
                header('Pragma: no-cache');
                header('Cache-Control: no-cache, must-revalidate');
                header('Location: ' . $ssoBuiltUrl);
                exit();
            }
        }else {
            return redirect('/sp/home');
        }
    }

    public function logout($entity) {
        session_start();

        // $returnTo = 'https://saml.dira.work/sp/sls';
        $returnTo = null;
        $parameters = array();
        $nameId = null;
        $sessionIndex = null;
        $nameIdFormat = null;
        $nameIdNameQualifier = null;
        $nameIdSPNameQualifier = null;

        if (isset($_SESSION['samlNameId'])) {
            $nameId = $_SESSION['samlNameId'];
        }
        if (isset($_SESSION['samlSessionIndex'])) {
            $sessionIndex = $_SESSION['samlSessionIndex'];
        }
        if (isset($_SESSION['samlNameIdFormat'])) {
            $nameIdFormat = $_SESSION['samlNameIdFormat'];
        }
        if (isset($_SESSION['samlNameIdNameQualifier'])) {
            $nameIdNameQualifier = $_SESSION['samlNameIdNameQualifier'];
        }
        if (isset($_SESSION['samlNameIdSPNameQualifier'])) {
            $nameIdSPNameQualifier = $_SESSION['samlNameIdSPNameQualifier'];
        }

        // dd([
        //     'returnTo' => $returnTo,
        //     'parameters' => $parameters,
        //     'nameId' => $nameId,
        //     'sessionIndex' => $sessionIndex,
        //     'nameIdFormat' => $nameIdFormat,
        //     'nameIdNameQualifier' => $nameIdNameQualifier,
        //     'nameIdSPNameQualifier' => $nameIdSPNameQualifier,
        // ]);

        $auth = $this->getAuth($entity);
        // $auth->logout();   // Method that sent the Logout Request.  
        // $auth->logout($returnTo, $parameters, $nameId, $sessionIndex, false, $nameIdFormat, $nameIdNameQualifier, $nameIdSPNameQualifier);
        $sloBuiltUrl = $auth->logout($returnTo, $parameters, $nameId, $sessionIndex, true, $nameIdFormat, $nameIdNameQualifier, $nameIdSPNameQualifier);
        // $this->logout($returnTo, $parameters, $nameId, $sessionIndex, $stay, $nameIdFormat, $nameIdNameQualifier, $nameIdSPNameQualifier)
        // $sloBuiltUrl = $auth->logout(null, $paramters, $nameId, $sessionIndex, true);
        $_SESSION['LogoutRequestID'] = $auth->getLastRequestID();
        header('Pragma: no-cache');
        header('Cache-Control: no-cache, must-revalidate');
        header('Location: ' . $sloBuiltUrl);
        exit();
    }
    
    // callback login
    public function acs(Request $request)
    {
        $entity = $this->extractEntity($request->RelayState);
        // dd($request->all());

        // IMPORTANT: This is required in order to be able // to store the user data in the session.
        session_start();

        $auth = $this->getAuth($entity);

        if (isset($_SESSION) && isset($_SESSION['AuthNRequestID'])) {
            $requestID = $_SESSION['AuthNRequestID'];
        } else {
            $requestID = null;
        }

        $auth->processResponse($requestID);
        unset($_SESSION['AuthNRequestID']);

        $errors = $auth->getErrors();

        if (!empty($errors)) {
            echo '<p>', implode(', ', $errors), '</p>';
            exit();
        }

        if (!$auth->isAuthenticated()) {
            echo "<p>Not authenticated</p>";
            exit();
        }
        
        $_SESSION['samlUserdata'] = $auth->getAttributes();
        $_SESSION['samlNameId'] = $auth->getNameId();
        $_SESSION['samlNameIdFormat'] = $auth->getNameIdFormat();
        $_SESSION['samlNameIdNameQualifier'] = $auth->getNameIdNameQualifier();
        $_SESSION['samlNameIdSPNameQualifier'] = $auth->getNameIdSPNameQualifier();
        $_SESSION['samlSessionIndex'] = $auth->getSessionIndex();

        // if (isset($_POST['RelayState']) && \OneLogin\Saml2\Utils::getSelfURL() != $_POST['RelayState']) {
        //     // dd('RelayState', $_POST['RelayState']);
        //     // To avoid 'Open Redirect' attacks, before execute the 
        //     // redirection confirm the value of $_POST['RelayState'] is a // trusted URL.
        //     // $auth->redirectTo($_POST['RelayState']);
        //     return redirect('/sp/home');
        // }

        $attributes = $_SESSION['samlUserdata'];
        $nameId = $_SESSION['samlNameId'];


        return redirect('/sp/home');

        echo '<h1>Identified user: '. htmlentities($nameId) .'</h1>';

        if (!empty($attributes)) {
            echo '<h2>'._('User attributes:').'</h2>';
            echo '<table><thead><th>'._('Name').'</th><th>'._('Values').'</th></thead><tbody>';
            foreach ($attributes as $attributeName => $attributeValues) {
                echo '<tr><td>' . htmlentities($attributeName) . '</td><td><ul>';
                foreach ($attributeValues as $attributeValue) {
                    echo '<li>' . htmlentities($attributeValue) . '</li>';
                }
                echo '</ul></td></tr>';
            }
            echo '</tbody></table>';
        } else {
            echo _('No attributes found.');
        }
    }

    // callback logout
    public function sls(Request $request)
    {
        // dd('callbackSls', $request->all());
        $entity = $this->extractEntity($request->RelayState);

        // IMPORTANT: This is required in order to be able // to close the user session.
        session_start();

        $auth = $this->getAuth($entity);

        if (isset($_SESSION) && isset($_SESSION['LogoutRequestID'])) {
            $requestID = $_SESSION['LogoutRequestID'];
        } else {
            $requestID = null;
        }


        // dd([
        //     'message' => 'callbackSls',
        //     'requestID' => $requestID,
        // ]);

        // $url = $auth->processSLO(false, $requestID, false);
        // dd($url);

        $auth->processSLO(false, $requestID, true);
        
        $errors = $auth->getErrors();

        // dd( compact('errors'));
        // dd($errors);

        if (empty($errors)) {
            // echo 'Sucessfully logged out';
            return redirect('sp/app');
        } else {
            echo implode(', ', $errors);
        }
    }

    public function extractEntity($url)
    {
        $pos = strrpos($url, "/");
        $entity = substr($url, $pos+1);
        return $entity;
    }
}
