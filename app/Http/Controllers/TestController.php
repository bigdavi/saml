<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestController extends Controller
{
    public function __invoke()
    {
        $idp = file_get_contents(base_path('/docs/idp/sonda.json'));
        $idp = json_decode($idp, true);
        $config = config('saml');
        $config['idp'] = $idp;
        dd($config);
    }
}
