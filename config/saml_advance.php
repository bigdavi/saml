<?php

return array(

    // Service Provider Data that we are deploying
    'sp' => array(
        // Identifier of the SP entity  (must be a URI)
        // 'entityId' => 'http://localhost:8080',
        'entityId' =>  env('APP_URL').'/sp/metadata',
        
        // Specifies info about where and how the <AuthnResponse> message MUST be
        // returned to the requester, in this case our SP.
        'assertionConsumerService' => array(
            // URL Location where the <Response> from the IdP will be returned
            'url' => env('APP_URL').'/sp/acs',
            // SAML protocol binding to be used when returning the <Response>
            // message.  Onelogin Toolkit supports for this endpoint the
            // HTTP-POST binding only
            'binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
        ),
        // If you need to specify requested attributes, set a
        // attributeConsumingService. nameFormat, attributeValue and
        // friendlyName can be omitted. Otherwise remove this section.
        "attributeConsumingService" => array(
            "serviceName" => "SP test",
            "serviceDescription" => "Test Service",
            "requestedAttributes" => array(
                array(
                    "name" => "",
                    "isRequired" => false,
                    "nameFormat" => "",
                    "friendlyName" => "",
                    "attributeValue" => ""
                )
            )
        ),
        // Specifies info about where and how the <Logout Response> message MUST be
        // returned to the requester, in this case our SP.
        'singleLogoutService' => array(
            // URL Location where the <Response> from the IdP will be returned
            'url' => env('APP_URL').'/sp/sls',

            // SAML protocol binding to be used when returning the <Response>
            // message.  Onelogin Toolkit supports for this endpoint the
            // HTTP-Redirect binding only
            'binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
        ),
        // Specifies constraints on the name identifier to be used to
        // represent the requested subject.
        // Take a look on lib/Saml2/Constants.php to see the NameIdFormat supported
        'NameIDFormat' => 'urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress',

        // Usually x509cert and privateKey of the SP are provided by files placed at
        // the certs folder. But we can also provide them with the following parameters
        'x509cert'  => 'MIIFBzCCA2+gAwIBAgIUR0Ck+XUHMmis52gVFFJDLagldEswDQYJKoZIhvcNAQEL
                        BQAwgZIxCzAJBgNVBAYTAlBFMQ0wCwYDVQQIDARMaW1hMQ0wCwYDVQQHDARMaW1h
                        MRAwDgYDVQQKDAdCaWdkYXZpMRYwFAYDVQQLDA1GaXJtYSBkaWdpdGFsMRQwEgYD
                        VQQDDAtiaWdkYXZpLmNvbTElMCMGCSqGSIb3DQEJARYWcm9tZWwuZGlhekBiaWdk
                        YXZpLmNvbTAeFw0yMjA2MTUxNjIxNTNaFw0zMjA2MTQxNjIxNTNaMIGSMQswCQYD
                        VQQGEwJQRTENMAsGA1UECAwETGltYTENMAsGA1UEBwwETGltYTEQMA4GA1UECgwH
                        QmlnZGF2aTEWMBQGA1UECwwNRmlybWEgZGlnaXRhbDEUMBIGA1UEAwwLYmlnZGF2
                        aS5jb20xJTAjBgkqhkiG9w0BCQEWFnJvbWVsLmRpYXpAYmlnZGF2aS5jb20wggGi
                        MA0GCSqGSIb3DQEBAQUAA4IBjwAwggGKAoIBgQDcqKxI8CkyiWtKez/i8h1gfsnd
                        BzJYXhGYhlhJRepfvUDB1GKI5SxX5oFTY64j8sfAvgK6oYM/jxG05T5gfCtDbinc
                        Q52bE6U88fS4hOkoufMe2wfd9rOVSQD7jeeuWXNFmwJd99QeJeo1d7wT0woRRLEz
                        y81ADFqcclKDVJNuW1J1eCYyfPwXB5sLBzJQsgqh24C8PgrgMJ+cM2R7bybutlKX
                        K1idlCqIIj/b+tCujaEJcO/iohJadOAeAHkkdeEp986dGE1x+HqrVEH1vtVkuIXf
                        TV4HSr6TxRiqqD3e54veQKLaOyoZJNXQ3z3l06N1732LfSv91CszlA3oJ3xT+Lfi
                        JREzD34Ueved8bK20bKVFNheBkchHw6r0ZWDK83SjoNrqRDG+95izBWzFxXoq2UW
                        ScF0ytiHZ7goCx5COOmzYv6hfEETXmq639CR7MSgfgNzeQWlOgXPKY56zLTihMqw
                        xWZf5Wn5YbcX7dGAlMq+61IRsXz9ZWkHpamUjF0CAwEAAaNTMFEwHQYDVR0OBBYE
                        FAFAz6X88S8RgWKk28KyCSgcTezQMB8GA1UdIwQYMBaAFAFAz6X88S8RgWKk28Ky
                        CSgcTezQMA8GA1UdEwEB/wQFMAMBAf8wDQYJKoZIhvcNAQELBQADggGBAMHhjxxl
                        3rvrugkyod6MekzMSpktx3PiydaK2D9Fexhn6dtdHjRlW98O+vKRJjE3hnJdvnq4
                        7ZJ6LTbAo2/TeKnY5YpYpug5e3+EMZOf9Xn9kCmOYJZu36C9JRhBA8hfl2QrTTmK
                        7py5KwtIxFxhbBNDMHJ1SOYq6//3xAAB0vMnzHAL+1FD5lTpE8WUr7+qb+2tktu7
                        t6L0nwZYdmD9Rewvc0Xbq727coni84MhoXucMTvbNGSQ5bpaHUSIwz7HFsknr9qd
                        aJZmpE7BCJhvRpSdlMRLd0Pp6+WoxsdU1xtEQ8XNJPPlMMUTApLanXGP9jLo3MTp
                        Iz4UaQ8wnSCHDknza390DI7/Fo3H8Xk++XsYTWH5NTgEQjaJOiOV3VNIOoTHWU+6
                        auLuHXj3CfuldF7wwh7WU9gfMFW2S8aW6MEnMZR/AwKgqIehQP3VkjE75LQq3IAU
                        QsFEUxqFa00wxfyCz/WhohASUgesLbQlNr2HrHYKNFzWEU8uaT67IIyBaw==',

        'privateKey' => 'MIIG/AIBADANBgkqhkiG9w0BAQEFAASCBuYwggbiAgEAAoIBgQDcqKxI8CkyiWtK
                        ez/i8h1gfsndBzJYXhGYhlhJRepfvUDB1GKI5SxX5oFTY64j8sfAvgK6oYM/jxG0
                        5T5gfCtDbincQ52bE6U88fS4hOkoufMe2wfd9rOVSQD7jeeuWXNFmwJd99QeJeo1
                        d7wT0woRRLEzy81ADFqcclKDVJNuW1J1eCYyfPwXB5sLBzJQsgqh24C8PgrgMJ+c
                        M2R7bybutlKXK1idlCqIIj/b+tCujaEJcO/iohJadOAeAHkkdeEp986dGE1x+Hqr
                        VEH1vtVkuIXfTV4HSr6TxRiqqD3e54veQKLaOyoZJNXQ3z3l06N1732LfSv91Csz
                        lA3oJ3xT+LfiJREzD34Ueved8bK20bKVFNheBkchHw6r0ZWDK83SjoNrqRDG+95i
                        zBWzFxXoq2UWScF0ytiHZ7goCx5COOmzYv6hfEETXmq639CR7MSgfgNzeQWlOgXP
                        KY56zLTihMqwxWZf5Wn5YbcX7dGAlMq+61IRsXz9ZWkHpamUjF0CAwEAAQKCAYBI
                        nxa8lpXodQOjqraW+XojpPbI0LKIFMWTvjtWOjVttvqWEFExnsMeVtWfXUIVwLRv
                        QdniFH/+rhZ0w7HozVTrx1jtji9Tt359dWBGTi/yYGicche/vI7UBgfVwplKb1QZ
                        x29a/9KG7ZGMcVCr8ZvxnPr5Ag0WFDrKHHH9mRtAibu0v4tKuIm7BpzHX2H23vHS
                        3UwcKAsqR7ZwERXz7dwRWl4KNJDgo7/vt93uhzihCinC0rGQlCiYLrkIBr3X+xc4
                        Z402NqArjQT2Y03icDG6NwTHxvdfO1yjN/xAVYERWhLPtiw/PjxQLQZqwOBJnR4g
                        4694Yz5LXlUgnIybDeAfl1LWNSSogf7JScyzrYyX2uU+3GT1ybkvWssxjoNFU2IP
                        cTQyD6nvoFEuZouf0Z97rwjt1ltlCfHhkyZDATXsTnZw30nryCYGu/OVmAz8LoCt
                        HKVerf/acDy6oL2o+r0HhtNGXNfOLLQtPU9a2TsDI9EqmPPH6PMYQ+isEliorUEC
                        gcEA7q9SAYDYvUovJPRFzuLcCO0+dYFl70+lXAYJgbYJd1kEwdXUb1kYsGiwnVtw
                        2RPmQiRmvPhpoNaq7PmmUd1stjHFNOaOh9KXGi5DsqTJGYoQV3KYrr0Dyr24AJS2
                        +Vx9vY55LGCtyMuGWAPTIlCU/LRB9ZD5Gr4/evC3cA6aDIu/svNW7mgyh/QTg/kY
                        PCnNOhoDIJrNoYD7sfkQpBCILWZzxPrPIDa82jxHbIB3YJ6GHS0dXpzpiTkJ6ELy
                        YKvxAoHBAOyqlm/wli+RrEyI4cUzfkX7aEfXxuIHLr2z+t9Ka96ZmXmHYA5AqAtM
                        jxMGttxtCiwFetAl5R607cdfXcChu/6WGjex6ccubjtw5CMCkAj78dYeHVWAdy4G
                        uZeG69tspGaSZkmKBYu13wS4c0t7XrZDryuaKHwrEPQxT4a5k6glshMS0bDYnqVo
                        lI7p0qVD9YqHDmV2Uvm3XS+atvSIeUI3sZ7wXhN9CVHhRCH3dcZjuCWRvZK6nyRg
                        9p4GwjqDLQKBwHZSIuzr2oW7iLNhBFoSy9EBCr1lcAq6CNcdhwjuHLM1Rc5xGeY4
                        3KxWuXbIJaD25GZVdTioTHSW9hSTjYd3pLSM553ZzT43fwBTfuvIFIi8kG1JObcl
                        rAONGPHwxuPGne6h+h+trXikq+xluPyBxdmf4jLBszs2pHFlcwnxsxwVCurDBo0Y
                        yjPrIuGBUipA7qOm0XYQR2+QaTVIqp8qMteOukUjsBngCK6Gw0FeTc9vxjwgaTbF
                        wl9sDyU8SrrxgQKBwCz+0dM+fo9UttlmPOZtP9i2T8E4+QsIKo7OpSfG0nNj6JX+
                        cvjCujBLwffwEtwdrw1njU39pm4MIBkDvKNfHIruptVn8fZ4kWSjtfwiBJhwri3z
                        U8hpwD7z8F0qvbTeFvOZafEZWKyTS29uUh1Lu1oTzD4R9RiGZ5bFuUT4C2awpAaU
                        UWG6DY+NLpFtSys4nHpV6xrHQEdR4VmnpJkhSUyZoh+Bu6Eg6KC0n2uFzNKHKv+Z
                        kd1PteoItVqeFldhkQKBwCPtFLnc/x3UKzU6ZvCGfHww44xCZ1IvhPyrRBHRAXey
                        A2mbQfbBovuF5UJfDNhtaGEwOBaraa6QwEciRswphvPNZYb9YhDiMN3U7LDaL8j6
                        +OJtQKByarjuOnVBeZLR825v/po7KdiCQsAQBtXGx5xURWnktCBItWPg65kHWjys
                        NMOZZ6tHwL7xPp9eoQg368zyzXTAaqueF0YVmdKYEq4dn95OgJzxUkLfIjrIer4j
                        2+Fv1RPzyOIIUgmSo84Aqg==',

                        /*
         * Key rollover
         * If you plan to update the SP x509cert and privateKey
         * you can define here the new x509cert and it will be 
         * published on the SP metadata so Identity Providers can
         * read them and get ready for rollover.
         */
        // 'x509certNew' => '',
    ),

    // Identity Provider Data that we want connect with our SP
    'idp' => array(
        // Identifier of the IdP entity  (must be a URI)
        'entityId' => 'http://sts.sonda.com/adfs/services/trust',
        // SSO endpoint info of the IdP. (Authentication Request protocol)
        'singleSignOnService' => array(
            // URL Target of the IdP where the SP will send the Authentication Request Message
            'url' => 'https://sts.sonda.com/adfs/ls/',
            // SAML protocol binding to be used when returning the <Response>
            // message.  Onelogin Toolkit supports for this endpoint the
            // HTTP-Redirect binding only
            'binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
        ),
        // SLO endpoint info of the IdP.
        'singleLogoutService' => array(
            // URL Location of the IdP where the SP will send the SLO Request
            'url' => 'https://sts.sonda.com/adfs/ls/',
            // URL location of the IdP where the SP SLO Response will be sent (ResponseLocation)
            // if not set, url for the SLO Request will be used
            'responseUrl' => '',
            // SAML protocol binding to be used when returning the <Response>
            // message.  Onelogin Toolkit supports for this endpoint the
            // HTTP-Redirect binding only
            'binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
        ),
        // Public x509 certificate of the IdP
        'x509cert' => '',
        /*
         *  Instead of use the whole x509cert you can use a fingerprint in
         *  order to validate the SAMLResponse, but we don't recommend to use
         *  that method on production since is exploitable by a collision
         *  attack.
         *  (openssl x509 -noout -fingerprint -in "idp.crt" to generate it,
         *   or add for example the -sha256 , -sha384 or -sha512 parameter)
         *
         *  If a fingerprint is provided, then the certFingerprintAlgorithm is required in order to
         *  let the toolkit know which Algorithm was used. Possible values: sha1, sha256, sha384 or sha512
         *  'sha1' is the default value.
         */
        // 'certFingerprint' => '',
        // 'certFingerprintAlgorithm' => 'sha1',

        /* In some scenarios the IdP uses different certificates for
         * signing/encryption, or is under key rollover phase and more 
         * than one certificate is published on IdP metadata.
         * In order to handle that the toolkit offers that parameter.
         * (when used, 'x509cert' and 'certFingerprint' values are
         * ignored).
         */
        // 'x509certMulti' => array(
        //      'signing' => array(
        //          0 => '<cert1-string>',
        //      ),
        //      'encryption' => array(
        //          0 => '<cert2-string>',
        //      )
        // ),


        'x509certMulti' => [
            'encryption' => [
                0 => 'MIIC3DCCAcSgAwIBAgIQMF85iosXLINP4syhbysSKjANBgkqhkiG9w0BAQsFADAqMSgwJgYDVQQDEx9BREZTIEVuY3J5cHRpb24gLSBzdHMuc29uZGEuY29tMB4XDTIwMDUyNDIxMTE0NVoXDTIxMDUyNDIxMTE0NVowKjEoMCYGA1UEAxMfQURGUyBFbmNyeXB0aW9uIC0gc3RzLnNvbmRhLmNvbTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBALdPP2L6xPlTJjlpVqJFoiQp8qVXyciKNeQGAOhSVbevF3goQlI6TEUvTvKgl6hKnbC8EmqUiN5pjAcgHlMSaNsGugeWqCoYRKXIRJsLRNaC4DoxfSz89T0JobOrr3rVJjuTh+2NSS5FUCekc35YpthVR7w3k6rNBDvDft8eabm7O6k0gNGr6AP3g+kzLFEijuoaKR0o8qsI2OQUYbh8h0b/OZndquAH/lqcAzgZbhQLiwMlo1fSpl/T5phCMm6rB32/gM7h1GDDtp/S1iquhIaQ2+uY7pdVRylSFtK0qVqIMB2mrR2Wy0suJQP7K4kNw4c0Aj+N7Pidfk957qUv028CAwEAATANBgkqhkiG9w0BAQsFAAOCAQEArMclHuZ9oF/VpJWymSA+fJJq+ICqS/f4llNC1Cg5vzA60JXLQULMWPv5Dkd7qwlGrn7gSHWG2NcyuSI+UgFLkpXgkq38u9/9IUNsv5Ln4HFAL7BHhS2NOZzBIeSkNOwUs6qBQpop8xtciqqulk6b3zyc1J2VhkXnU0kT+VJCLlMrJqQdqrDDQK4+ThNJxRBsDG/f7gcYdlxrIJde7/G35TnESYZ9zVU08OEecBl+fSCIInmtA9KLMm2vVnibcA/kHqunKg/frK+NnCegBP2pe3TxPuMcO0gDwKP6jc1xxpxXaHWBwXd+M63qk6gINBJfkuCLXH7jL3IqdKEbelDp1w==',
            ],
            'signing' => [
                0 => 'MIIC1jCCAb6gAwIBAgIQEV5M+2K2rIRAH/FNZcic0DANBgkqhkiG9w0BAQsFADAnMSUwIwYDVQQDExxBREZTIFNpZ25pbmcgLSBzdHMuc29uZGEuY29tMB4XDTE5MDYxNDEwMTg1MVoXDTIwMDYxMzEwMTg1MVowJzElMCMGA1UEAxMcQURGUyBTaWduaW5nIC0gc3RzLnNvbmRhLmNvbTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBALCxib0QGmR141IZowS11sgMZc0Ewl9nR2sTUX/trNAJFrgsYDcsi8D7YcO8dfjDFeoT+AVSJAfGGdsjIuPhLOKBj+eK+wc5IJKCTtE0N7p0pM5VXj2keLP7MPncxSZVoz6wfU3iEsgwWrMZS5WTi2GTyHfVCyYqMtLabeVhvTTBFeSY/Be1XLUM4T8+l04AcsZYFo550O83p4GAMtieLI78cXBtzdIX/LvokiBa0fOxh+k2EZWgj5F22Mif5YjhkXsIqcrTb+6745PnHVoO3J+fi8XeNAdw0Hm1Xfivp2aj1gsjUJZDNEMN2LtmAdNyYVc7n6RPqPX/XUE1th72gDkCAwEAATANBgkqhkiG9w0BAQsFAAOCAQEAXf9/ulOFetrXKKPvQcSlKac2krMkczsr9jhr3lpxqdxIDCJ5J4Z90v/kv3GyxshK0x6S7fclosTCRC2BPEQ0tG8NIVOqbvcUqKZpuqtEB+1xCGxoAyU02bha+7Vhtw5vv2tjl02pbhg1xgLems/LE+QjJP2uOHH7EhRNVKzysHW+myn1wEMySvrIB7VgJ+z23y8vUnvViUNMeATyKxH9Lk69T+DdCsaQRMxVQXgbfcdj2ZOjr1jv6pM1OfYHYeyE0buW3SwpsuMIKyyPht6/kWhYNXI08tGXpljthWbaitUqmycZpr/yeextBD+7JMuXvrYxvbWzPeki3vXkdQSvww==',
            ],
            'signing' => [
                0 => 'MIIC1jCCAb6gAwIBAgIQEii+iaUBSLZPz3ilygLXqzANBgkqhkiG9w0BAQsFADAnMSUwIwYDVQQDExxBREZTIFNpZ25pbmcgLSBzdHMuc29uZGEuY29tMB4XDTIwMDUyNDIxMTE1N1oXDTIxMDUyNDIxMTE1N1owJzElMCMGA1UEAxMcQURGUyBTaWduaW5nIC0gc3RzLnNvbmRhLmNvbTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKUSvrh9UxBz8Q0tcjRDhHHZ/Rh1tFNwfnRD+yoLFORrowIvxrt2k7tI9M4n5Ym8y+aVONMj3so0zulExbT/1NFAIB/13/Eam8BJkjLLVgeA2mljukeuvMI+yGu+DKo1NrxaE0HN59nOtApu/XLTrmNJx1wrtOpDWg5u4oHXL23W/i8T/jJ0zN2GFcxD0vYcXFgTNVNklhbKZGXj9G2qX4ghGP2KgmUE0a402i8sjIzc8nB51FxtqdeQLG2yiPwzmxhKXes+OpR9ocabWRaX56dpnsOu2dwG4nfHuuatjCEu6kyBn2kQTFZ5RKBUXsbG3s651ArZczKk9OCTYa/hIJ8CAwEAATANBgkqhkiG9w0BAQsFAAOCAQEAYpql+Svidi7QgiOn25wi/+Wb++0DnoiSkXkEQveyRh7MbhTnh1zF9fKHFeNFrjvmk9smnF8GmHj5bjXGx2fFSlcqpCLKbMKXxywPPM5DBAzHdQAB1Z6eq3asmfGVBEBL2e4LTj39TsrL/MhHDg7TldmHZaSN90nzJQ5qaV3K1EebTSrkfFnPr/7yj+z/iEu93CRO8OoObAUO6bSElUPGFVQvnGs6uUSZJ0hFWn27Kk9iHl1F/6yfalxrxewZ+lQA6bcoj1xpLGyIoDNXpr82KUr4uyeC7k72A2xmVvxvCbwe93MnnmsllnbZSbeh4xaHvSXHyRd3eti5PhYnv0Nrxw==',
            ],
        ],
    ),

    // Compression settings
    // Handle if the getRequest/getResponse methods will return the Request/Response deflated.
    // But if we provide a $deflate boolean parameter to the getRequest or getResponse
    // method it will have priority over the compression settings.
    'compress' => array(
        'requests' => true,
        'responses' => true
    ),

    // Security settings
    'security' => array(

        /** signatures and encryptions offered */

        // Indicates that the nameID of the <samlp:logoutRequest> sent by this SP
        // will be encrypted.
        'nameIdEncrypted' => false,

        // Indicates whether the <samlp:AuthnRequest> messages sent by this SP
        // will be signed.              [The Metadata of the SP will offer this info]
        'authnRequestsSigned' => false,

        // Indicates whether the <samlp:logoutRequest> messages sent by this SP
        // will be signed.
        'logoutRequestSigned' => false,

        // Indicates whether the <samlp:logoutResponse> messages sent by this SP
        // will be signed.
        'logoutResponseSigned' => false,

        /* Sign the Metadata
         False || True (use sp certs) || array (
                                                    'keyFileName' => 'metadata.key',
                                                    'certFileName' => 'metadata.crt'
                                               )
                                      || array (
                                                    'x509cert' => '',
                                                    'privateKey' => ''
                                               )
        */
        'signMetadata' => false,


        /** signatures and encryptions required **/

        // Indicates a requirement for the <samlp:Response>, <samlp:LogoutRequest> and
        // <samlp:LogoutResponse> elements received by this SP to be signed.
        'wantMessagesSigned' => false,

        // Indicates a requirement for the <saml:Assertion> elements received by
        // this SP to be encrypted.
        'wantAssertionsEncrypted' => false,

        // Indicates a requirement for the <saml:Assertion> elements received by
        // this SP to be signed.        [The Metadata of the SP will offer this info]
        'wantAssertionsSigned' => false,

        // Indicates a requirement for the NameID element on the SAMLResponse received
        // by this SP to be present.
        // 'wantNameId' => true,
        'wantNameId' => true,

        // Indicates a requirement for the NameID received by
        // this SP to be encrypted.
        'wantNameIdEncrypted' => false,

        // Authentication context.
        // Set to false and no AuthContext will be sent in the AuthNRequest,
        // Set true or don't present this parameter and you will get an AuthContext 'exact' 'urn:oasis:names:tc:SAML:2.0:ac:classes:PasswordProtectedTransport'
        // Set an array with the possible auth context values: array('urn:oasis:names:tc:SAML:2.0:ac:classes:Password', 'urn:oasis:names:tc:SAML:2.0:ac:classes:X509'),
        'requestedAuthnContext' => false,

        // Allows the authn comparison parameter to be set, defaults to 'exact' if
        // the setting is not present.
        'requestedAuthnContextComparison' => 'exact',

        // Indicates if the SP will validate all received xmls.
        // (In order to validate the xml, 'strict' and 'wantXMLValidation' must be true).
        'wantXMLValidation' => true,

        // If true, SAMLResponses with an empty value at its Destination
        // attribute will not be rejected for this fact.
        'relaxDestinationValidation' => false,

        // If true, Destination URL should strictly match to the address to
        // which the response has been sent.
        // Notice that if 'relaxDestinationValidation' is true an empty Destintation
        // will be accepted.
        'destinationStrictlyMatches' => false,

        // If true, the toolkit will not raised an error when the Statement Element
        // contain atribute elements with name duplicated
        'allowRepeatAttributeName' => false,

        // If true, SAMLResponses with an InResponseTo value will be rejectd if not
        // AuthNRequest ID provided to the validation method.
        'rejectUnsolicitedResponsesWithInResponseTo' => false,

        // Algorithm that the toolkit will use on signing process. Options:
        //    'http://www.w3.org/2000/09/xmldsig#rsa-sha1'
        //    'http://www.w3.org/2000/09/xmldsig#dsa-sha1'
        //    'http://www.w3.org/2001/04/xmldsig-more#rsa-sha256'
        //    'http://www.w3.org/2001/04/xmldsig-more#rsa-sha384'
        //    'http://www.w3.org/2001/04/xmldsig-more#rsa-sha512'
        // Notice that rsa-sha1 is a deprecated algorithm and should not be used
        'signatureAlgorithm' => 'http://www.w3.org/2001/04/xmldsig-more#rsa-sha256',

        // Algorithm that the toolkit will use on digest process. Options:
        //    'http://www.w3.org/2000/09/xmldsig#sha1'
        //    'http://www.w3.org/2001/04/xmlenc#sha256'
        //    'http://www.w3.org/2001/04/xmldsig-more#sha384'
        //    'http://www.w3.org/2001/04/xmlenc#sha512'
        // Notice that sha1 is a deprecated algorithm and should not be used
        'digestAlgorithm' => 'http://www.w3.org/2001/04/xmlenc#sha256',

        // Algorithm that the toolkit will use for encryption process. Options:
        // 'http://www.w3.org/2001/04/xmlenc#tripledes-cbc'
        // 'http://www.w3.org/2001/04/xmlenc#aes128-cbc'
        // 'http://www.w3.org/2001/04/xmlenc#aes192-cbc'
        // 'http://www.w3.org/2001/04/xmlenc#aes256-cbc'
        // 'http://www.w3.org/2009/xmlenc11#aes128-gcm'
        // 'http://www.w3.org/2009/xmlenc11#aes192-gcm'
        // 'http://www.w3.org/2009/xmlenc11#aes256-gcm';
        // Notice that aes-cbc are not consider secure anymore so should not be used
        'encryption_algorithm' => 'http://www.w3.org/2009/xmlenc11#aes128-gcm',

        // ADFS URL-Encodes SAML data as lowercase, and the toolkit by default uses
        // uppercase. Turn it True for ADFS compatibility on signature verification
        'lowercaseUrlencoding' => false,
    ),

    // Contact information template, it is recommended to suply a technical and support contacts
    'contactPerson' => array(
        'technical' => array(
            'givenName' => 'Romel Diaz',
            'emailAddress' => 'romeldira95@gmail.com'
        ),
        'support' => array(
            'givenName' => 'Developer',
            'emailAddress' => 'romel.diaz@bigdavi.com'
        ),
    ),

    // Organization information template, the info in en_US lang is recomended, add more if required
    'organization' => array(
        'en-US' => array(
            'name' => 'Bigdavi SA',
            'displayname' => 'Bigdavi SA',
            'url' => 'bigdavi.com'
        ),
    ),
);


/* Interoperable SAML 2.0 Web Browser SSO Profile [saml2int]   http://saml2int.org/profile/current

   'authnRequestsSigned' => false,    // SP SHOULD NOT sign the <samlp:AuthnRequest>,
                                      // MUST NOT assume that the IdP validates the sign
   'wantAssertionsSigned' => true,
   'wantAssertionsEncrypted' => true, // MUST be enabled if SSL/HTTPs is disabled
   'wantNameIdEncrypted' => false,
*/
