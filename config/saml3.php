<?php

return array(

    


    // If 'strict' is True, then the PHP Toolkit will reject unsigned
    // or unencrypted messages if it expects them signed or encrypted
    // Also will reject the messages if not strictly follow the SAML
    // standard: Destination, NameId, Conditions ... are validated too.
    'strict' => true,

    // Enable debug mode (to print errors)
    'debug' => true,

    // Set a BaseURL to be used instead of try to guess
    // the BaseURL of the view that process the SAML Message.
    // Ex. http://sp.example.com/
    //     http://example.com/sp/
    // 'baseurl' => env('APP_URL', 'http://localhost'),

    // Service Provider Data that we are deploying
    'sp' => array(
        // Identifier of the SP entity  (must be a URI)
        // 'entityId' => 'http://localhost:8080',
        'entityId' =>  env('APP_URL').'/sp/metadata',
        
        // Specifies info about where and how the <AuthnResponse> message MUST be
        // returned to the requester, in this case our SP.
        'assertionConsumerService' => array(
            // URL Location where the <Response> from the IdP will be returned
            'url' => env('APP_URL').'/sp/acs',
            // SAML protocol binding to be used when returning the <Response>
            // message.  Onelogin Toolkit supports for this endpoint the
            // HTTP-POST binding only
            'binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
        ),
        // If you need to specify requested attributes, set a
        // attributeConsumingService. nameFormat, attributeValue and
        // friendlyName can be omitted. Otherwise remove this section.
        "attributeConsumingService" => array(
            "serviceName" => "SP test",
            "serviceDescription" => "Test Service",
            "requestedAttributes" => array(
                array(
                    "name" => "",
                    "isRequired" => false,
                    "nameFormat" => "",
                    "friendlyName" => "",
                    "attributeValue" => ""
                )
            )
        ),
        // Specifies info about where and how the <Logout Response> message MUST be
        // returned to the requester, in this case our SP.
        'singleLogoutService' => array(
            // URL Location where the <Response> from the IdP will be returned
            'url' => env('APP_URL').'/sp/sls',

            // SAML protocol binding to be used when returning the <Response>
            // message.  Onelogin Toolkit supports for this endpoint the
            // HTTP-Redirect binding only
            'binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
        ),
        // Specifies constraints on the name identifier to be used to
        // represent the requested subject.
        // Take a look on lib/Saml2/Constants.php to see the NameIdFormat supported
        'NameIDFormat' => 'urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified',
        // 'NameIDFormat' => 'urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress',
        // 'NameIDFormat' => 'urn:oasis:names:tc:SAML:1.1:nameid-format:persistent',



        // const NAMEID_EMAIL_ADDRESS = 'urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress';
        // const NAMEID_X509_SUBJECT_NAME = 'urn:oasis:names:tc:SAML:1.1:nameid-format:X509SubjectName';
        // const NAMEID_WINDOWS_DOMAIN_QUALIFIED_NAME = 'urn:oasis:names:tc:SAML:1.1:nameid-format:WindowsDomainQualifiedName';
        // const NAMEID_UNSPECIFIED = 'urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified';
        // const NAMEID_KERBEROS   = 'urn:oasis:names:tc:SAML:2.0:nameid-format:kerberos';
        // const NAMEID_ENTITY     = 'urn:oasis:names:tc:SAML:2.0:nameid-format:entity';
        // const NAMEID_TRANSIENT  = 'urn:oasis:names:tc:SAML:2.0:nameid-format:transient';
        // const NAMEID_PERSISTENT = 'urn:oasis:names:tc:SAML:2.0:nameid-format:persistent';
        // const NAMEID_ENCRYPTED = 'urn:oasis:names:tc:SAML:2.0:nameid-format:encrypted';


        // <NameIDFormat>urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress</NameIDFormat>
        // <NameIDFormat>urn:oasis:names:tc:SAML:2.0:nameid-format:persistent</NameIDFormat>
        // <NameIDFormat>urn:oasis:names:tc:SAML:2.0:nameid-format:transient</NameIDFormat>


        // Usually x509cert and privateKey of the SP are provided by files placed at
        // the certs folder. But we can also provide them with the following parameters


        'x509cert'  => 'MIIFBzCCA2+gAwIBAgIUR0Ck+XUHMmis52gVFFJDLagldEswDQYJKoZIhvcNAQEL
                        BQAwgZIxCzAJBgNVBAYTAlBFMQ0wCwYDVQQIDARMaW1hMQ0wCwYDVQQHDARMaW1h
                        MRAwDgYDVQQKDAdCaWdkYXZpMRYwFAYDVQQLDA1GaXJtYSBkaWdpdGFsMRQwEgYD
                        VQQDDAtiaWdkYXZpLmNvbTElMCMGCSqGSIb3DQEJARYWcm9tZWwuZGlhekBiaWdk
                        YXZpLmNvbTAeFw0yMjA2MTUxNjIxNTNaFw0zMjA2MTQxNjIxNTNaMIGSMQswCQYD
                        VQQGEwJQRTENMAsGA1UECAwETGltYTENMAsGA1UEBwwETGltYTEQMA4GA1UECgwH
                        QmlnZGF2aTEWMBQGA1UECwwNRmlybWEgZGlnaXRhbDEUMBIGA1UEAwwLYmlnZGF2
                        aS5jb20xJTAjBgkqhkiG9w0BCQEWFnJvbWVsLmRpYXpAYmlnZGF2aS5jb20wggGi
                        MA0GCSqGSIb3DQEBAQUAA4IBjwAwggGKAoIBgQDcqKxI8CkyiWtKez/i8h1gfsnd
                        BzJYXhGYhlhJRepfvUDB1GKI5SxX5oFTY64j8sfAvgK6oYM/jxG05T5gfCtDbinc
                        Q52bE6U88fS4hOkoufMe2wfd9rOVSQD7jeeuWXNFmwJd99QeJeo1d7wT0woRRLEz
                        y81ADFqcclKDVJNuW1J1eCYyfPwXB5sLBzJQsgqh24C8PgrgMJ+cM2R7bybutlKX
                        K1idlCqIIj/b+tCujaEJcO/iohJadOAeAHkkdeEp986dGE1x+HqrVEH1vtVkuIXf
                        TV4HSr6TxRiqqD3e54veQKLaOyoZJNXQ3z3l06N1732LfSv91CszlA3oJ3xT+Lfi
                        JREzD34Ueved8bK20bKVFNheBkchHw6r0ZWDK83SjoNrqRDG+95izBWzFxXoq2UW
                        ScF0ytiHZ7goCx5COOmzYv6hfEETXmq639CR7MSgfgNzeQWlOgXPKY56zLTihMqw
                        xWZf5Wn5YbcX7dGAlMq+61IRsXz9ZWkHpamUjF0CAwEAAaNTMFEwHQYDVR0OBBYE
                        FAFAz6X88S8RgWKk28KyCSgcTezQMB8GA1UdIwQYMBaAFAFAz6X88S8RgWKk28Ky
                        CSgcTezQMA8GA1UdEwEB/wQFMAMBAf8wDQYJKoZIhvcNAQELBQADggGBAMHhjxxl
                        3rvrugkyod6MekzMSpktx3PiydaK2D9Fexhn6dtdHjRlW98O+vKRJjE3hnJdvnq4
                        7ZJ6LTbAo2/TeKnY5YpYpug5e3+EMZOf9Xn9kCmOYJZu36C9JRhBA8hfl2QrTTmK
                        7py5KwtIxFxhbBNDMHJ1SOYq6//3xAAB0vMnzHAL+1FD5lTpE8WUr7+qb+2tktu7
                        t6L0nwZYdmD9Rewvc0Xbq727coni84MhoXucMTvbNGSQ5bpaHUSIwz7HFsknr9qd
                        aJZmpE7BCJhvRpSdlMRLd0Pp6+WoxsdU1xtEQ8XNJPPlMMUTApLanXGP9jLo3MTp
                        Iz4UaQ8wnSCHDknza390DI7/Fo3H8Xk++XsYTWH5NTgEQjaJOiOV3VNIOoTHWU+6
                        auLuHXj3CfuldF7wwh7WU9gfMFW2S8aW6MEnMZR/AwKgqIehQP3VkjE75LQq3IAU
                        QsFEUxqFa00wxfyCz/WhohASUgesLbQlNr2HrHYKNFzWEU8uaT67IIyBaw==',

        'privateKey' => 'MIIG/AIBADANBgkqhkiG9w0BAQEFAASCBuYwggbiAgEAAoIBgQDcqKxI8CkyiWtK
                        ez/i8h1gfsndBzJYXhGYhlhJRepfvUDB1GKI5SxX5oFTY64j8sfAvgK6oYM/jxG0
                        5T5gfCtDbincQ52bE6U88fS4hOkoufMe2wfd9rOVSQD7jeeuWXNFmwJd99QeJeo1
                        d7wT0woRRLEzy81ADFqcclKDVJNuW1J1eCYyfPwXB5sLBzJQsgqh24C8PgrgMJ+c
                        M2R7bybutlKXK1idlCqIIj/b+tCujaEJcO/iohJadOAeAHkkdeEp986dGE1x+Hqr
                        VEH1vtVkuIXfTV4HSr6TxRiqqD3e54veQKLaOyoZJNXQ3z3l06N1732LfSv91Csz
                        lA3oJ3xT+LfiJREzD34Ueved8bK20bKVFNheBkchHw6r0ZWDK83SjoNrqRDG+95i
                        zBWzFxXoq2UWScF0ytiHZ7goCx5COOmzYv6hfEETXmq639CR7MSgfgNzeQWlOgXP
                        KY56zLTihMqwxWZf5Wn5YbcX7dGAlMq+61IRsXz9ZWkHpamUjF0CAwEAAQKCAYBI
                        nxa8lpXodQOjqraW+XojpPbI0LKIFMWTvjtWOjVttvqWEFExnsMeVtWfXUIVwLRv
                        QdniFH/+rhZ0w7HozVTrx1jtji9Tt359dWBGTi/yYGicche/vI7UBgfVwplKb1QZ
                        x29a/9KG7ZGMcVCr8ZvxnPr5Ag0WFDrKHHH9mRtAibu0v4tKuIm7BpzHX2H23vHS
                        3UwcKAsqR7ZwERXz7dwRWl4KNJDgo7/vt93uhzihCinC0rGQlCiYLrkIBr3X+xc4
                        Z402NqArjQT2Y03icDG6NwTHxvdfO1yjN/xAVYERWhLPtiw/PjxQLQZqwOBJnR4g
                        4694Yz5LXlUgnIybDeAfl1LWNSSogf7JScyzrYyX2uU+3GT1ybkvWssxjoNFU2IP
                        cTQyD6nvoFEuZouf0Z97rwjt1ltlCfHhkyZDATXsTnZw30nryCYGu/OVmAz8LoCt
                        HKVerf/acDy6oL2o+r0HhtNGXNfOLLQtPU9a2TsDI9EqmPPH6PMYQ+isEliorUEC
                        gcEA7q9SAYDYvUovJPRFzuLcCO0+dYFl70+lXAYJgbYJd1kEwdXUb1kYsGiwnVtw
                        2RPmQiRmvPhpoNaq7PmmUd1stjHFNOaOh9KXGi5DsqTJGYoQV3KYrr0Dyr24AJS2
                        +Vx9vY55LGCtyMuGWAPTIlCU/LRB9ZD5Gr4/evC3cA6aDIu/svNW7mgyh/QTg/kY
                        PCnNOhoDIJrNoYD7sfkQpBCILWZzxPrPIDa82jxHbIB3YJ6GHS0dXpzpiTkJ6ELy
                        YKvxAoHBAOyqlm/wli+RrEyI4cUzfkX7aEfXxuIHLr2z+t9Ka96ZmXmHYA5AqAtM
                        jxMGttxtCiwFetAl5R607cdfXcChu/6WGjex6ccubjtw5CMCkAj78dYeHVWAdy4G
                        uZeG69tspGaSZkmKBYu13wS4c0t7XrZDryuaKHwrEPQxT4a5k6glshMS0bDYnqVo
                        lI7p0qVD9YqHDmV2Uvm3XS+atvSIeUI3sZ7wXhN9CVHhRCH3dcZjuCWRvZK6nyRg
                        9p4GwjqDLQKBwHZSIuzr2oW7iLNhBFoSy9EBCr1lcAq6CNcdhwjuHLM1Rc5xGeY4
                        3KxWuXbIJaD25GZVdTioTHSW9hSTjYd3pLSM553ZzT43fwBTfuvIFIi8kG1JObcl
                        rAONGPHwxuPGne6h+h+trXikq+xluPyBxdmf4jLBszs2pHFlcwnxsxwVCurDBo0Y
                        yjPrIuGBUipA7qOm0XYQR2+QaTVIqp8qMteOukUjsBngCK6Gw0FeTc9vxjwgaTbF
                        wl9sDyU8SrrxgQKBwCz+0dM+fo9UttlmPOZtP9i2T8E4+QsIKo7OpSfG0nNj6JX+
                        cvjCujBLwffwEtwdrw1njU39pm4MIBkDvKNfHIruptVn8fZ4kWSjtfwiBJhwri3z
                        U8hpwD7z8F0qvbTeFvOZafEZWKyTS29uUh1Lu1oTzD4R9RiGZ5bFuUT4C2awpAaU
                        UWG6DY+NLpFtSys4nHpV6xrHQEdR4VmnpJkhSUyZoh+Bu6Eg6KC0n2uFzNKHKv+Z
                        kd1PteoItVqeFldhkQKBwCPtFLnc/x3UKzU6ZvCGfHww44xCZ1IvhPyrRBHRAXey
                        A2mbQfbBovuF5UJfDNhtaGEwOBaraa6QwEciRswphvPNZYb9YhDiMN3U7LDaL8j6
                        +OJtQKByarjuOnVBeZLR825v/po7KdiCQsAQBtXGx5xURWnktCBItWPg65kHWjys
                        NMOZZ6tHwL7xPp9eoQg368zyzXTAaqueF0YVmdKYEq4dn95OgJzxUkLfIjrIer4j
                        2+Fv1RPzyOIIUgmSo84Aqg==',

                        /*
         * Key rollover
         * If you plan to update the SP x509cert and privateKey
         * you can define here the new x509cert and it will be 
         * published on the SP metadata so Identity Providers can
         * read them and get ready for rollover.
         */
        // 'x509certNew' => '',

        'security' => [
            'wantNameId' => true,
        ],
    ),

    // Identity Provider Data that we want connect with our SP
    'idp' => array(
        // Identifier of the IdP entity  (must be a URI)
        'entityId' => 'http://sts.sonda.com/adfs/services/trust',
        // SSO endpoint info of the IdP. (Authentication Request protocol)
        'singleSignOnService' => array(
            // URL Target of the IdP where the SP will send the Authentication Request Message
            'url' => 'https://sts.sonda.com/adfs/ls',
            // 'url' => 'https://sts.sonda.com/auth2/login',
            // SAML protocol binding to be used when returning the <Response>
            // message.  Onelogin Toolkit supports for this endpoint the
            // HTTP-Redirect binding only
            'binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
        ),
        // SLO endpoint info of the IdP.
        'singleLogoutService' => array(
            // URL Location of the IdP where the SP will send the SLO Request
            'url' => 'https://sts.sonda.com/adfs/ls/',
            // 'url' => 'https://sts.sonda.com/adfs/ls/?wa=wsignout1.0',
            // 'url' => 'https://sts.sonda.com/adfs/oauth2/logout/',
            // 'url' => 'https://sts.sonda.com/adfs/oauth2/logout/?wa=wsignout1.0',

            // Ubicación de la URL del IdP donde se enviará la respuesta del SLO del SP (ResponseLocation).
            // si no se establece, se utilizará la URL para la solicitud de SLO
            // 'responseUrl' => 'https://sts.sonda.com/adfs/ls/',
            'responseUrl' => 'https://sts.sonda.com/adfs/ls',
            // 'responseUrl' => 'https://sts.sonda.com/adfs/oauth2/logout',

            // SAML protocol binding to be used when returning the <Response>
            // message.  Onelogin Toolkit supports for this endpoint the
            // HTTP-Redirect binding only
            'binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
        ),
        // Public x509 certificate of the IdP
        // 'x509cert' => '',
        'x509cert' => 'MIIC1jCCAb6gAwIBAgIQdKDBKEoGMa5M4VcXm3KDJjANBgkqhkiG9w0BAQsFADAnMSUwIwYDVQQDExxBREZTIFNpZ25pbmcgLSBzdHMuc29uZGEuY29tMB4XDTIyMDQxNTEwMDkwOVoXDTIzMDQxNTEwMDkwOVowJzElMCMGA1UEAxMcQURGUyBTaWduaW5nIC0gc3RzLnNvbmRhLmNvbTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAJj4ZtjcVjTjX4XFz2nkrTcX0ru9MHlEDE4YdjkcNrob+rmPJtv8sQBQVGwipD5dPbAjqqgBKhkNyBeuwhQdCYiAZh4YzrNqAmbaGidHSYrJ8EabUrg1HcdKOj/25RObPy0ZcHpdriFnUSp3NZaol5WlN4iilFX/lAr/u/33XKu+h+UFsuwnfCeG0y8ens7x7PkL3Hena7Ml7Z6WwSXo05Pdrsx38HMegO1W2Kumu8Jo9jScMf0YQGS2AXpvqDxL16sNQKYCqZeOcMVrihu0LQP/+yp5Rn+2+JQz0lcvMAnjGf5IzPSUB2N6prKQmL3ET4pKm0PqQ3wWuuCeTU9AqCkCAwEAATANBgkqhkiG9w0BAQsFAAOCAQEAcLEqxlHxwPU/7WblAmIrDtMfm0kRHEkuPn7yhDWCDHK+TwLIBWf/Uq8i7PSgjQEQ1nNzRKMtWKeey3FkfIAeqNLwiCButvMR2mNPONTzhqAX8bxAXhGwPDmEtlIu3qrtpcGf3KUX2I+Jhzb+Zyldk5Ma1A8dcrzZRYoHqjWSnEmWvlAjaQUC1NT/cY3GtQlGYgGEWIh0CSsYr+I2IrvSU9cgpRnNkAlv5bj0KgKy2dN9vaUwiLi4SqoKXyC7G5ck4/MW8OXbcF+lo9N3nKnVl6yVBhW5JD+FJcCppmKm4cItJA65wpTgU8d6JVbVatG3gAvJES5mgvHxAjfvS3PiiA==',
        /*
         *  Instead of use the whole x509cert you can use a fingerprint in
         *  order to validate the SAMLResponse, but we don't recommend to use
         *  that method on production since is exploitable by a collision
         *  attack.
         *  (openssl x509 -noout -fingerprint -in "idp.crt" to generate it,
         *   or add for example the -sha256 , -sha384 or -sha512 parameter)
         *
         *  If a fingerprint is provided, then the certFingerprintAlgorithm is required in order to
         *  let the toolkit know which Algorithm was used. Possible values: sha1, sha256, sha384 or sha512
         *  'sha1' is the default value.
         */
        // 'certFingerprint' => '',
        // 'certFingerprintAlgorithm' => 'sha1',

        /* In some scenarios the IdP uses different certificates for
         * signing/encryption, or is under key rollover phase and more 
         * than one certificate is published on IdP metadata.
         * In order to handle that the toolkit offers that parameter.
         * (when used, 'x509cert' and 'certFingerprint' values are
         * ignored).
         */
        // 'x509certMulti' => array(
        //      'signing' => array(
        //          0 => '<cert1-string>',
        //      ),
        //      'encryption' => array(
        //          0 => '<cert2-string>',
        //      )
        // ),

        // 'x509certMulti' => [
        //     'encryption' => [
        //         // 0 => 'MIIC3DCCAcSgAwIBAgIQMF85iosXLINP4syhbysSKjANBgkqhkiG9w0BAQsFADAqMSgwJgYDVQQDEx9BREZTIEVuY3J5cHRpb24gLSBzdHMuc29uZGEuY29tMB4XDTIwMDUyNDIxMTE0NVoXDTIxMDUyNDIxMTE0NVowKjEoMCYGA1UEAxMfQURGUyBFbmNyeXB0aW9uIC0gc3RzLnNvbmRhLmNvbTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBALdPP2L6xPlTJjlpVqJFoiQp8qVXyciKNeQGAOhSVbevF3goQlI6TEUvTvKgl6hKnbC8EmqUiN5pjAcgHlMSaNsGugeWqCoYRKXIRJsLRNaC4DoxfSz89T0JobOrr3rVJjuTh+2NSS5FUCekc35YpthVR7w3k6rNBDvDft8eabm7O6k0gNGr6AP3g+kzLFEijuoaKR0o8qsI2OQUYbh8h0b/OZndquAH/lqcAzgZbhQLiwMlo1fSpl/T5phCMm6rB32/gM7h1GDDtp/S1iquhIaQ2+uY7pdVRylSFtK0qVqIMB2mrR2Wy0suJQP7K4kNw4c0Aj+N7Pidfk957qUv028CAwEAATANBgkqhkiG9w0BAQsFAAOCAQEArMclHuZ9oF/VpJWymSA+fJJq+ICqS/f4llNC1Cg5vzA60JXLQULMWPv5Dkd7qwlGrn7gSHWG2NcyuSI+UgFLkpXgkq38u9/9IUNsv5Ln4HFAL7BHhS2NOZzBIeSkNOwUs6qBQpop8xtciqqulk6b3zyc1J2VhkXnU0kT+VJCLlMrJqQdqrDDQK4+ThNJxRBsDG/f7gcYdlxrIJde7/G35TnESYZ9zVU08OEecBl+fSCIInmtA9KLMm2vVnibcA/kHqunKg/frK+NnCegBP2pe3TxPuMcO0gDwKP6jc1xxpxXaHWBwXd+M63qk6gINBJfkuCLXH7jL3IqdKEbelDp1w==',
        //         // 0 => 'b738Ra273sktkBCg+bNdwhxpHooIE9kEXnvwCw6hWto+JrkqSPbKgi7YASqU31brI+wrV8iyQEYfAoH1loDx3d2kwMahngYrkWR1Foiuc9PwKThAftG/PvCdilspLazWUaPZO/NuM0E9PzE3SYkQH/xrd3sJYhcPl4dL19XsMhp4gTU+3LHf3hO+Bx37gDyu+u9TK5/lMvk6WgLtUJD+iVvZ3t+iX6fVMFBU4JhEXaHq9eqt99OcrSaNsWl8Ng3K+g/2FGRQcVdeUvae9LNYqlvKEHGfQN8dGxjYygcc1r3477x1SHCn9BlrRJDRg+v762CkF/c0FckDPIxuB0TVww==',
        //     ],
        //     'signing' => [
        //         0 => 'MIIC1jCCAb6gAwIBAgIQEV5M+2K2rIRAH/FNZcic0DANBgkqhkiG9w0BAQsFADAnMSUwIwYDVQQDExxBREZTIFNpZ25pbmcgLSBzdHMuc29uZGEuY29tMB4XDTE5MDYxNDEwMTg1MVoXDTIwMDYxMzEwMTg1MVowJzElMCMGA1UEAxMcQURGUyBTaWduaW5nIC0gc3RzLnNvbmRhLmNvbTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBALCxib0QGmR141IZowS11sgMZc0Ewl9nR2sTUX/trNAJFrgsYDcsi8D7YcO8dfjDFeoT+AVSJAfGGdsjIuPhLOKBj+eK+wc5IJKCTtE0N7p0pM5VXj2keLP7MPncxSZVoz6wfU3iEsgwWrMZS5WTi2GTyHfVCyYqMtLabeVhvTTBFeSY/Be1XLUM4T8+l04AcsZYFo550O83p4GAMtieLI78cXBtzdIX/LvokiBa0fOxh+k2EZWgj5F22Mif5YjhkXsIqcrTb+6745PnHVoO3J+fi8XeNAdw0Hm1Xfivp2aj1gsjUJZDNEMN2LtmAdNyYVc7n6RPqPX/XUE1th72gDkCAwEAATANBgkqhkiG9w0BAQsFAAOCAQEAXf9/ulOFetrXKKPvQcSlKac2krMkczsr9jhr3lpxqdxIDCJ5J4Z90v/kv3GyxshK0x6S7fclosTCRC2BPEQ0tG8NIVOqbvcUqKZpuqtEB+1xCGxoAyU02bha+7Vhtw5vv2tjl02pbhg1xgLems/LE+QjJP2uOHH7EhRNVKzysHW+myn1wEMySvrIB7VgJ+z23y8vUnvViUNMeATyKxH9Lk69T+DdCsaQRMxVQXgbfcdj2ZOjr1jv6pM1OfYHYeyE0buW3SwpsuMIKyyPht6/kWhYNXI08tGXpljthWbaitUqmycZpr/yeextBD+7JMuXvrYxvbWzPeki3vXkdQSvww==',
        //     ],
        //     // C1
        //     'signing' => [
        //         // 0 => 'MIIC1jCCAb6gAwIBAgIQEii+iaUBSLZPz3ilygLXqzANBgkqhkiG9w0BAQsFADAnMSUwIwYDVQQDExxBREZTIFNpZ25pbmcgLSBzdHMuc29uZGEuY29tMB4XDTIwMDUyNDIxMTE1N1oXDTIxMDUyNDIxMTE1N1owJzElMCMGA1UEAxMcQURGUyBTaWduaW5nIC0gc3RzLnNvbmRhLmNvbTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKUSvrh9UxBz8Q0tcjRDhHHZ/Rh1tFNwfnRD+yoLFORrowIvxrt2k7tI9M4n5Ym8y+aVONMj3so0zulExbT/1NFAIB/13/Eam8BJkjLLVgeA2mljukeuvMI+yGu+DKo1NrxaE0HN59nOtApu/XLTrmNJx1wrtOpDWg5u4oHXL23W/i8T/jJ0zN2GFcxD0vYcXFgTNVNklhbKZGXj9G2qX4ghGP2KgmUE0a402i8sjIzc8nB51FxtqdeQLG2yiPwzmxhKXes+OpR9ocabWRaX56dpnsOu2dwG4nfHuuatjCEu6kyBn2kQTFZ5RKBUXsbG3s651ArZczKk9OCTYa/hIJ8CAwEAATANBgkqhkiG9w0BAQsFAAOCAQEAYpql+Svidi7QgiOn25wi/+Wb++0DnoiSkXkEQveyRh7MbhTnh1zF9fKHFeNFrjvmk9smnF8GmHj5bjXGx2fFSlcqpCLKbMKXxywPPM5DBAzHdQAB1Z6eq3asmfGVBEBL2e4LTj39TsrL/MhHDg7TldmHZaSN90nzJQ5qaV3K1EebTSrkfFnPr/7yj+z/iEu93CRO8OoObAUO6bSElUPGFVQvnGs6uUSZJ0hFWn27Kk9iHl1F/6yfalxrxewZ+lQA6bcoj1xpLGyIoDNXpr82KUr4uyeC7k72A2xmVvxvCbwe93MnnmsllnbZSbeh4xaHvSXHyRd3eti5PhYnv0Nrxw==',
        //         0 => 'MIIC1jCCAb6gAwIBAgIQdKDBKEoGMa5M4VcXm3KDJjANBgkqhkiG9w0BAQsFADAnMSUwIwYDVQQDExxBREZTIFNpZ25pbmcgLSBzdHMuc29uZGEuY29tMB4XDTIyMDQxNTEwMDkwOVoXDTIzMDQxNTEwMDkwOVowJzElMCMGA1UEAxMcQURGUyBTaWduaW5nIC0gc3RzLnNvbmRhLmNvbTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAJj4ZtjcVjTjX4XFz2nkrTcX0ru9MHlEDE4YdjkcNrob+rmPJtv8sQBQVGwipD5dPbAjqqgBKhkNyBeuwhQdCYiAZh4YzrNqAmbaGidHSYrJ8EabUrg1HcdKOj/25RObPy0ZcHpdriFnUSp3NZaol5WlN4iilFX/lAr/u/33XKu+h+UFsuwnfCeG0y8ens7x7PkL3Hena7Ml7Z6WwSXo05Pdrsx38HMegO1W2Kumu8Jo9jScMf0YQGS2AXpvqDxL16sNQKYCqZeOcMVrihu0LQP/+yp5Rn+2+JQz0lcvMAnjGf5IzPSUB2N6prKQmL3ET4pKm0PqQ3wWuuCeTU9AqCkCAwEAATANBgkqhkiG9w0BAQsFAAOCAQEAcLEqxlHxwPU/7WblAmIrDtMfm0kRHEkuPn7yhDWCDHK+TwLIBWf/Uq8i7PSgjQEQ1nNzRKMtWKeey3FkfIAeqNLwiCButvMR2mNPONTzhqAX8bxAXhGwPDmEtlIu3qrtpcGf3KUX2I+Jhzb+Zyldk5Ma1A8dcrzZRYoHqjWSnEmWvlAjaQUC1NT/cY3GtQlGYgGEWIh0CSsYr+I2IrvSU9cgpRnNkAlv5bj0KgKy2dN9vaUwiLi4SqoKXyC7G5ck4/MW8OXbcF+lo9N3nKnVl6yVBhW5JD+FJcCppmKm4cItJA65wpTgU8d6JVbVatG3gAvJES5mgvHxAjfvS3PiiA==',
        //     ],
        // ],


        // 'security' => array(
        //     'nameIdEncrypted' => false,
        //     'authnRequestsSigned' => true,
        //     'logoutRequestSigned' => true,
        //     'logoutResponseSigned' => true,
        //     'signMetadata' => false,
        //     'wantMessagesSigned' => false,
        //     'wantAssertionsSigned' => true,
        //     'wantNameIdEncrypted' => false,
        //     'requestedAuthnContext' => true,
        //     // 'signatureAlgorithm' => 'http://www.w3.org/2001/04/xmldsig-more#rsa-sha256',
        //     // 'signatureAlgorithm' => 'http://www.w3.org/2001/04/xmldsig-more#rsa-sha1',
        // ),
    ),
    
);
