<?php
// https://www.samltool.com/idp_metadata.php
return [
    'security' => [
        // 'authnRequestsSigned' =>  true, //false
        // 'wantAssertionsSigned' => true, //false
        'wantNameId' => true,
        'logoutRequestSigned' => true,
        'logoutResponseSigned' => true,
    ],

    'strict' => true,

    'debug' => true,

    // 'baseurl' => env('APP_URL', 'http://localhost'),

    'sp' => [

        'entityId' =>  env('APP_URL').'/sp/metadata',

        'assertionConsumerService' => [
            'url' => env('APP_URL').'/sp/acs',
            'binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
        ],

        "attributeConsumingService" => [
            "serviceName" => "Davicloud - Docs",
            "serviceDescription" => "Plataforma de firma electronica y digital con valor legal",
            "requestedAttributes" => [
                [
                    "name" => "",
                    "isRequired" => false,
                    "nameFormat" => "",
                    "friendlyName" => "",
                    "attributeValue" => ""
                ],
            ],
        ],

        'singleLogoutService' => [
            'url' => env('APP_URL').'/sp/sls',
            'binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
        ],

        'NameIDFormat' => 'urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified',

        'x509cert' => file_get_contents(base_path('/docs/cert/bigdavi.crt')),
        'privateKey' => file_get_contents(base_path('/docs/cert/bigdavi.pem')),
    ],

    'idp' => [
        'entityId' => 'https://idp.example.test/adfs/services/trust',
        'singleSignOnService' => [
            'url' => 'https://idp.example.test/adfs/ls/',
            'binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
        ],
        'singleLogoutService' => [
            'url' => 'https://idp.example.test/adfs/ls/',
            'responseUrl' => '',
            'binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
        ],

        'x509cert' => 'CONTENTFILE',
    ],
    
];
