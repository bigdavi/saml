<?php

use App\Http\Controllers\SpController;
use App\Http\Controllers\TestController;
use Illuminate\Support\Facades\Route;


Route::prefix('sp')->group(function() {

    Route::get('/test', TestController::class);

    Route::get('/', function () {
        return view('welcome');
    });

    Route::get('app', [ SpController::class, 'viewApp' ]);
    Route::get('home', [ SpController::class, 'viewHome' ]);
    Route::get('metadata', [ SpController::class, 'viewMetadata' ]);
    Route::get('login/{entity}', [ SpController::class, 'login' ]);
    Route::get('logout/{entity}', [ SpController::class, 'logout' ]);
    // callbacks
    Route::post('acs', [ SpController::class, 'acs' ]);
    Route::get('sls', [ SpController::class, 'sls' ]);
});

