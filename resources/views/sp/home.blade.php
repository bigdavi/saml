@extends('layouts.sp')

@section('contents')
    <div class="jumbotron">
        <h1 class="display-4">Welcome!</h1>
        <p class="lead">
            @php
                if(!isset($_SESSION)){session_start();}
                $userData = $_SESSION['samlUserdata']?? null;

                echo "<ul>";
                foreach ($userData as $key => $userValue) {
                    echo "<li>$userValue[0]</li>";                  
                }
                echo "</ul>";
            @endphp
        </p>
        <a class="btn btn-danger btn-lg" href="{{ url('/sp/logout/'.encriptar(123)) }}" role="button">Logout Sonda</a>
    </div>
@endsection